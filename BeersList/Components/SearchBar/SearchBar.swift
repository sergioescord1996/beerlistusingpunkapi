//
//  SearchBar.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 14/01/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit

protocol SearchBarDelegate: class {
    func inputChanges(input: String)
}

class SearchBar: UIView {
    
    // MARK: Outlets
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var searchImage: UIImageView!
    
    // MARK: Variables
    weak var delegate: SearchBarDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        xibSetup()
        setupView()
    }
    
    func setupView() {
        
        //TextField
        textField.delegate = self
        textField.borderStyle = .roundedRect
        textField.textColor = .black
        textField.tintColor = .black
        textField.backgroundColor = .white
        let placeholder = NSAttributedString(string: "Introduce tu comida", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        textField.attributedPlaceholder = placeholder
        
        //Image
        let image = UIImage(imageLiteralResourceName: "search")
        searchImage.tintColor = .black
        searchImage.image = image
    }

}

extension SearchBar: UITextFieldDelegate {
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        delegate?.inputChanges(input: textField.text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing(true)
        return true
    }
}
