//
//  BeersListPresenter.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 13/01/2020.
//  Copyright (c) 2020 Sergio Escalante Ordoñez. All rights reserved.
//

protocol BeersListDisplayLogic: class {
    func setupView()
    func updateData()
}

protocol BeersListDataStore {
    var beerSelected: BeerModel? { get set }
}

class BeersListPresenter: BeersListPresenterLogic, BeersListDataStore {

    weak var view: (BeersListDisplayLogic & BeersListRouterLogic)?
    var beerSelected: BeerModel?
    var viewModel = BeersList.ViewModel()
//    var repository: RepositoryInterface = Repository()

    func setupView() {
        view?.setupView()
    }
    
    func searchBeers(food: String) {
        
        // Check pagination
        
        if viewModel.food != food {
            viewModel.perPage = 15
        }
        
        // Update model
        
        viewModel.food = food
        
        //Create params
        
        var params: [String: String] = [:]
        let foodFormatter = food.replacingOccurrences(of: " ", with: "_")
        params["food"] = foodFormatter
        params["per_page"] = "\(viewModel.perPage)"
        
        API.getBeersByFood(params: params, completion: { [weak self] (success, data, error) in
            if success {
                self?.viewModel.beers = data
            } else {
                #if DEBUG
                print(error)
                #endif
            }
            
            self?.view?.updateData()
        })
    }
    
    func getNumberOfRows() -> Int {
        return viewModel.beers?.count ?? 0
    }
    
    func getCellData(index: Int) -> BeersList.CellViewModel {
        
        guard let beer = viewModel.beers?[index] else {
            return BeersList.CellViewModel()
        }
        
        let data = BeersList.CellViewModel(imageUrl: beer.imageUrl ?? "", name: beer.name ?? "")
        
        return data
    }
    
    func updatePagination() {
        
        let beersCount = viewModel.beers?.count ?? 0
        
        if beersCount == viewModel.perPage && beersCount < 75 {
            viewModel.perPage += 15
            searchBeers(food: viewModel.food ?? "")
        }
    }
    
}
