//
//  BeersListViewController.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 13/01/2020.
//  Copyright (c) 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit

protocol BeersListPresenterLogic {
    func setupView()
    func searchBeers(food: String)
    func getNumberOfRows() -> Int
    func getCellData(index: Int) -> BeersList.CellViewModel
    func updatePagination()
}

class BeersListViewController: UIViewController {
    var presenter: BeersListPresenterLogic?
    var dataStore: BeersListDataStore?
    
    // MARK: - Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBar: SearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Setup
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupScene()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupScene()
    }
  
    func setupScene() {
        let viewController = self
        let presenter = BeersListPresenter()
        
        presenter.view = viewController
        viewController.dataStore = presenter
        self.presenter = presenter
    }
  
    // MARK: View lifecycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.setupView()
    }
}

// MARK: - Display Logic

extension BeersListViewController: BeersListDisplayLogic {

    func setupView() {
        
        //Title
        titleLabel.text = "Listado cervezas"
        
        //SearchBar
        searchBar.delegate = self
        
        //TableView
        tableView.backgroundColor = .white
        let nibCell = UINib(nibName: "BeerTableViewCell", bundle: nil)
        tableView.register(nibCell, forCellReuseIdentifier: "BeerTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 104
        
        presenter?.searchBeers(food: "")
    }
    
    func updateData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
}

// MARK: - Delegates


// MARK: - Router Logic

protocol BeersListRouterLogic: class {

}

protocol BeersListDataPass {
    var dataStore: BeersListDataStore? { get }
}

extension BeersListViewController: BeersListRouterLogic, BeersListDataPass {
    
    // MARK: Passing data
    
    //func passDataToSomewhere(source: BeersListDataStore?, destination: inout SomewhereDataStore?)
    //{
    //  destination?.name = source?.name
    //}
}

extension BeersListViewController: SearchBarDelegate {
    func inputChanges(input: String) {
        presenter?.searchBeers(food: input)
    }
}

extension BeersListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getNumberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BeerTableViewCell") as? BeerTableViewCell else {
            return UITableViewCell()
        }
        
        if let model = presenter?.getCellData(index: indexPath.row) {
            cell.updateUI(model: model)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
        
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            presenter?.updatePagination()
        }
    }
}
