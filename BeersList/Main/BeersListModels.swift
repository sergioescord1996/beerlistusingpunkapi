//
//  BeersListModels.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 13/01/2020.
//  Copyright (c) 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit

enum BeersList {
    
    struct CellViewModel {

        var beerImageUrl: String?
        var beerName: String?
        
        init() {
            beerImageUrl = ""
            beerName = ""
        }
        
        init(imageUrl: String, name: String) {
            beerImageUrl = imageUrl
            beerName = name
        }
    }
    
    struct ViewModel {
        var food: String?
        var beers: [BeerModel]?
        var perPage: Int = 15
        
        init() {
            beers = []
        }
        
        init(food: String, beers: [BeerModel]) {
            self.food = food
            self.beers = beers
        }
    }
    
}
