//
//  BeerTableViewCell.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 14/01/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit
import SDWebImage

class BeerTableViewCell: UITableViewCell {
    
    // MARK: Outlets
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = .clear
        
        itemLabel.textColor = .black
        itemLabel.font = UIFont.systemFont(ofSize: 24.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(model: BeersList.CellViewModel) {
//        self.itemImage.loadFromUrl(stringUrl: model.beerImageUrl ?? "")
        itemImage.sd_setImage(with: URL(string: model.beerImageUrl ?? ""), placeholderImage: UIImage(named: "warning"))
        self.itemLabel.text = model.beerName
    }
    
}
