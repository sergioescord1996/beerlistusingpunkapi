//
//  API.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 13/01/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation

class API {
    
    class func getBeersByFood(params: [String: String], completion: @escaping (Bool, [BeerModel], String) -> Void) {
        
        var params = params
        
//        params["per_page"] = "25" //Always the same beers per page
        
        if params["food"] == "" {
            params.removeValue(forKey: "food")
        }
                
        let request = APIManager.request("/beers?\(params.stringFromHttpParameters())", http_method: "GET")
        
        //Request to test when API return an error
//        let request = APIManager.request("/bee\(params.stringFromHttpParameters())", http_method: "GET")
        
        APIManager.sendRequest(request, completion: { (success, data, error) in
            
            if success, let objects = data as? [NSDictionary] {
                var beers: [BeerModel] = [] 
                
                for object in objects {
                    let beer = BeerModel(object)
                    beers.append(beer)
                }
                
                completion(true, beers, error)
            } else {
                completion(false, [], error)
            }
        })
    }
}
