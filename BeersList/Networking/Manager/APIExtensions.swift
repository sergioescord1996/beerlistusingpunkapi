//
//  APIExtensions.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 13/01/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation
import UIKit

extension URLRequest {
    
    mutating func addMultipartBody(_ params: [String: String]?, image: UIImage?, imageName: String) {
        
        let boundary = getBoundary()
        setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        httpBody = createBodyWithParameters(params, image: image, imageName: imageName, boundary: boundary)

    }
    
    private func createBodyWithParameters(_ parameters: [String: String]?, image: UIImage?, imageName: String, boundary: String) -> Data {
        
        let body = NSMutableData()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        if image != nil {
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(imageName)\"; filename=\"\(imageName).jpeg\"\r\n")
            body.appendString("Content-Type: image/jpg\r\n\r\n")
            body.append(image!.jpegData(compressionQuality: 0.9)!)
            body.appendString("\r\n")
            
        }
        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
    }
    
    private func getBoundary() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
}
