//
//  APIManager.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 13/01/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation
import UIKit

class APIManager {

    class func request(_ endpoint: String, http_method: String) -> URLRequest {
        
        let url = URL(string: "https://api.punkapi.com/v2\(endpoint)")
        var request:URLRequest = URLRequest(url: url!)
        request.httpMethod = http_method
        
        return request
        
    }
    
    class func sendRequest(_ request: URLRequest, completion: ((Bool,AnyObject?,String) -> Void)?){
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: {(data, response, error) in
            
            if(error == nil && data != nil){
                
                do {
          
                    let dict: NSDictionary? = (try JSONSerialization.jsonObject(with: data!, options: [])) as? NSDictionary
                    
                    if let dict = dict {
                        
                        if let _ = dict.object(forKey: "error") as? String {
                            let message = dict.object(forKey: "message") as? String ?? ""
                            completion!(false, dict, message)
                        }
                        
                        completion!(true, dict, "")
                    } else {
                         let dict: NSArray? = (try JSONSerialization.jsonObject(with: data!, options: [])) as? NSArray
                        completion!(true, dict, "")
                    }
                    
                } catch let caught as NSError {
                    
                    completion!(false, nil, caught.localizedDescription)
                    
                } catch {
                    
                    completion!(false, nil, "")
                }
                
            } else {
                
                completion!(false ,nil, (error?.localizedDescription)!)
            }
        })
        
        task.resume();
        
    }
    
    class func createBodyWithParameters(_ parameters: [String: String]?, image: UIImage?, imageName: String) -> Data {
        
        let body = NSMutableData()
        let boundary = generateBoundaryString()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        if image != nil {
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(imageName)\"; filename=\"\(imageName).jpeg\"\r\n")
            body.appendString("Content-Type: image/jpg\r\n\r\n")
            body.append(image!.jpegData(compressionQuality: 0.9)!)
            body.appendString("\r\n")
            
        }
        
        body.appendString("--\(boundary)--\r\n")
        return body as Data
    }
    
    class func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
}
