//
//  BeerModel.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 13/01/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation

class BeerModel {
    
    // MARK: Variables
    var id: NSNumber?
    var name: String?
    var description: String?
    var imageUrl: String?
    var foodPairing: [String]?
    
    // MARK: Constants
    
    // MARK: Init

    init(_ dic: NSDictionary) {
        id = dic.object(forKey: "id") as? NSNumber
        name = dic.object(forKey: "name") as? String ?? ""
        description = dic.object(forKey: "description") as? String ?? ""
        imageUrl = dic.object(forKey: "image_url") as? String
        foodPairing = dic.object(forKey: "food_pairing") as? [String] ?? []
    }
}
