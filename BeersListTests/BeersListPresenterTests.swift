//
//  BeersListPresenterTests.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 15/01/2020.
//  Copyright (c) 2020 Sergio Escalante Ordoñez. All rights reserved.
//

@testable import BeersList
import XCTest

class BeersListPresenterTests: XCTestCase {
    // MARK: Subject under test
  
    var sut: BeersListPresenter!
  
    // MARK: Test lifecycle
  
    override func setUp() {
        super.setUp()
        setupBeersListPresenter()
    }
  
    override func tearDown() {
        super.tearDown()
    }
  
    // MARK: Test setup
  
    func setupBeersListPresenter() {
        sut = BeersListPresenter()
    }
  
    // MARK: Spies
  
    class BeersListDisplayLogicSpy: BeersListDisplayLogic, BeersListRouterLogic {
        func updateData() {
            
        }
        
        var setupViewCalled = false
        
        func setupView() {
            setupViewCalled = true
        }
        
        // MARK: Router Spy Logic
    }
  
    // MARK: Tests
  
    func testSetup() {
        // Given
        let spy = BeersListDisplayLogicSpy()
        sut.view = spy
        
        // When
        sut.setupView()
        
        // Then
        XCTAssertTrue(spy.setupViewCalled, "setupView() should ask the view controller to setupView()")
    }
}
