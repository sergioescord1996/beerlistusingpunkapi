//
//  BeersListViewControllerTests.swift
//  BeersList
//
//  Created by Sergio Escalante Ordonez on 15/01/2020.
//  Copyright (c) 2020 Sergio Escalante Ordoñez. All rights reserved.
//

@testable import BeersList
import XCTest

class BeersListViewControllerTests: XCTestCase {
    // MARK: Subject under test
  
    var sut: BeersListViewController!
    var window: UIWindow!
  
    // MARK: Test lifecycle
  
    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupBeersListViewController()
    }
  
    override func tearDown() {
        window = nil
        super.tearDown()
    }
  
    // MARK: Test setup
  
    func setupBeersListViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "BeersList", bundle: bundle)
        sut = storyboard.instantiateInitialViewController() as? BeersListViewController
    }
  
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
  
    // MARK: Spies
  
    class BeersListPresenterLogicSpy: BeersListPresenterLogic {
        func searchBeers(food: String) {
            
        }
        
        func getNumberOfRows() -> Int {
            return 0
        }
        
        func getCellData(index: Int) -> BeersList.CellViewModel {
            return BeersList.CellViewModel()
        }
        
        var setupViewCalled = false

        func setupView() {
            setupViewCalled = true
        }
    }
  
  // MARK: Tests
  
    func testShouldSetupViewWhenViewIsLoaded() {
        // Given
        let spy = BeersListPresenterLogicSpy()
        sut.presenter = spy

        // When
        loadView()

        // Then
        XCTAssertTrue(spy.setupViewCalled, "viewDidLoad() should ask the presenter to setupView()")
    }
}
